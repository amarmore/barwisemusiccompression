# -*- coding: utf-8 -*-
"""
Created on Wed Apr 14 18:34:29 2021

@author: amarmore
"""

import numpy as np
import barwisemusiccompression.data_manipulation as dm
import barwisemusiccompression.autosimilarity_segmentation as as_seg
import barwisemusiccompression.model.errors as err

import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import tensorly as tl
import matplotlib.cm as cm
import copy
import random
torch.manual_seed(42)
random.seed(42)
np.random.seed(42)

# %% Dataloaders generation
def generate_dataloader(tensor_barwise, batch_size = None):
    """
    Generates a torch.DataLoader from the tensor spectrogram.

    Parameters
    ----------
    tensor_barwise : np.array tensor
        The tensor-spectrogram as a np.array.

    Returns
    -------
    data_loader : torch.DataLoader
        torch.DataLoader for this song and this tensor-spectrogram.
    """
    if batch_size == None:
        batch_size = tensor_barwise.shape[0]
    num_workers = 0
    nb_bars = tensor_barwise.shape[0]
    freq_len, subdivision_bars = tensor_barwise.shape[1], tensor_barwise.shape[2]

    barwise_spec = copy.deepcopy(tensor_barwise)
    
    barwise_spec = barwise_spec.reshape((nb_bars, 1, freq_len, subdivision_bars))
    data_loader = torch.utils.data.DataLoader(barwise_spec, batch_size=batch_size, num_workers=num_workers)
    
    return data_loader
    
def generate_flatten_dataloader(tensor_barwise, batch_size = None):
    if batch_size == None:
        batch_size = tensor_barwise.shape[0]
    num_workers = 0
    nb_bars = tensor_barwise.shape[0]
    freq_len, subdivision_bars = tensor_barwise.shape[1], tensor_barwise.shape[2]

    barwise_spec = copy.deepcopy(tensor_barwise)
    
    flatten_barwise_spec = barwise_spec.reshape((nb_bars, freq_len*subdivision_bars), order="C")
    flatten_simplet_data_loader = torch.utils.data.DataLoader(flatten_barwise_spec, batch_size=batch_size, num_workers=num_workers)
    
    return flatten_simplet_data_loader
    
# %% Deterministic initializations for networks
def seeded_weights_init_kaiming(m): 
    """
    Determinstic initialization of weights with Kaiming uniform distribution.

    Parameters
    ----------
    m : torch.nn
        A layer of the network.
    """       
    # if isinstance(m, nn.BatchNorm1d):
    #     nn.init.zeros_(m.bias)
    #     nn.init.ones_(m.weight)
    if isinstance(m, nn.Linear) or isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
        torch.manual_seed(42)
        nn.init.kaiming_uniform_(m.weight, mode='fan_in', nonlinearity='relu')
        #nn.init.kaiming_uniform_(m.weight, mode='fan_out', nonlinearity='relu')
        if m.bias is not None:
            nn.init.zeros_(m.bias)

            
def seeded_weights_init_xavier(m):
    """
    Determinstic initialization of weights with Xavier uniform distribution.

    Parameters
    ----------
    m : torch.nn
        A layer of the network.
    """    
    # if isinstance(m, nn.BatchNorm1d):
    #     nn.init.zeros_(m.bias)
    #     nn.init.ones_(m.weight)
    if isinstance(m, nn.Linear) or isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d) or isinstance(m, nn.Conv2d) or isinstance(m, nn.ConvTranspose2d):
        torch.manual_seed(42)
        nn.init.xavier_uniform_(m.weight)
        if m.bias is not None:
            nn.init.zeros_(m.bias)
        
def weights_init_sparse(m):
    """
    Determinstic initialization of weights with sparse initialization.

    Parameters
    ----------
    m : torch.nn
        A layer of the network.
    """   
    if isinstance(m, nn.Conv1d) or isinstance(m, nn.Linear) or isinstance(m, nn.ConvTranspose1d):
        torch.nn.init.sparse_(m.weight, sparsity = 0.1)
        
def weights_ones(m):
    """
    Determinstic initialization of weights with layers full of ones.

    Parameters
    ----------
    m : torch.nn
        A layer of the network.
    """   
    if isinstance(m, nn.Linear):# or isinstance(m, nn.ConvTranspose2d):
        nn.init.ones_(m.weight) 
        
# %% Triplet losees
class TripletLoss(nn.Module):
    """
    Triplet Loss class, following the Triplet Loss paradigm. See [1] for details.
        
    Comes from: https://www.kaggle.com/hirotaka0122/triplet-loss-with-pytorch
    
    References
    ----------
    [1] Ho, K., Keuper, J., Pfreundt, F. J., & Keuper, M. (2021, January). 
    Learning embeddings for image clustering: An empirical study of triplet loss approaches. 
    In 2020 25th International Conference on Pattern Recognition (ICPR) (pp. 87-94). IEEE.
    """
    
    def __init__(self, margin=1.0):
        """
        Constructor of the loss.

        Parameters
        ----------
        margin : float, optional
            Margin for the triplet loss. The default is 1.0.

        Returns
        -------
        None.

        """
        super(TripletLoss, self).__init__()
        self.margin = margin
        
    def calc_euclidean(self, x1, x2):
        return (x1 - x2).pow(2).sum(1)
    
    def forward(self, anchor, positive, negative):
        distance_positive = self.calc_euclidean(anchor, positive)
        distance_negative = self.calc_euclidean(anchor, negative)
        losses = torch.relu(distance_positive - distance_negative + self.margin)
        return losses.mean()
    
class TripletLossDoubleMargin(nn.Module):
    """
    Triplet Loss with positive and negative margins, following the work of [1]
    
    References
    ----------
    [1] Ho, K., Keuper, J., Pfreundt, F. J., & Keuper, M. (2021, January). 
    Learning embeddings for image clustering: An empirical study of triplet loss approaches. 
    In 2020 25th International Conference on Pattern Recognition (ICPR) (pp. 87-94). IEEE.
    """
    def __init__(self, pos_margin=1.0, neg_margin = 3.0):
        """
        Constructor of the loss.

        Parameters
        ----------
        pos_margin : float, optional
            Margin for positive examples. The default is 1.0.
        neg_margin : float, optional
            Margin for negative examples. The default is 3.0.

        Returns
        -------
        None.

        """
        super(TripletLossDoubleMargin, self).__init__()
        self.pos_margin = pos_margin
        self.neg_margin = neg_margin
        
    def calc_euclidean(self, x1, x2):
        return (x1 - x2).pow(2).sum(1)
    
    def forward(self, anchor, positive, negative):
        distance_positive = self.calc_euclidean(anchor, positive)
        distance_negative = self.calc_euclidean(anchor, negative)
        losses = torch.relu(self.neg_margin - distance_negative) + torch.relu(distance_positive - self.pos_margin)
        return losses.mean()
    
# %% Beta-divergence loss
class BetaDivergenceLoss(nn.Module):
    """
    Some inspiration was taken from https://github.com/yoyololicon/pytorch-NMF/blob/master/torchnmf/metrics.py
    """

    def __init__(self, beta = 1, eps = 1e-12):
        super(BetaDivergenceLoss, self).__init__()
        self.beta = beta
        self.eps = eps
        
    def calc_beta_div(self, x1, x2):
        if self.beta == 1:
            log_prod = torch.mul(x1, (x1.add(self.eps).log() - x2.add(self.eps).log()))
            return (torch.add(torch.sub(log_prod, x1), x2)).mean()
        elif self.beta == 0:
            x1_div_x2 = torch.div(x1, x2.add(self.eps)).add(self.eps)
            return torch.sub(x1_div_x2, x1_div_x2.log()).sub(1).mean()
        else:
            if self.beta < 0:
                x1 = x1.add(self.eps)
                x2 = x2.add(self.eps)
            elif self.beta < 1:
                x2 = x2.add(self.eps)
            num =  torch.sub(torch.add(x1.pow(self.beta), x2.pow(self.beta).mul(self.beta - 1)), torch.mul(x1, x2.pow(self.beta-1)).mul(self.beta)).mean()
            return num / (self.beta * (self.beta - 1))

    def forward(self, x, y):
        if (x < 0).any():
            raise err.NegativeValuesNotAccepted("Negative values in the input of the network, can't perform beta divergence.")
        if (y < 0).any():
            raise err.NegativeValuesNotAccepted("Negative values in the output of the network, can't perform beta divergence.")
        losses = self.calc_beta_div(x, y)
        return losses.mean()
    