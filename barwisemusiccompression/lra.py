# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 18:47:32 2022

@author: amarmore
"""
from sklearn.decomposition import PCA
from sklearn.decomposition import KernelPCA
import nn_fac.nmf as NMF

def pca_projection(barwise_spectrogram, compression_dimension):
    pca = PCA(n_components=compression_dimension)
    pca_spec = pca.fit(barwise_spectrogram)
    return pca_spec.transform(barwise_spectrogram)

def kernel_pca_projection(barwise_spectrogram, compression_dimension, gamma, kernel='rbf'):
    k_pca = KernelPCA(eigen_solver = 'arpack', n_components=compression_dimension, kernel=kernel, gamma = gamma)
    return k_pca.fit_transform(barwise_spectrogram)

def nmf_projection(barwise_spectrogram, compression_dimension, update_rule = "hals", beta = 2, init = "nndsvd"):
    W_nmf, H_nmf = NMF.nmf(barwise_spectrogram, compression_dimension, update_rule = update_rule, beta = beta, init = init, return_costs = False)
    return W_nmf
